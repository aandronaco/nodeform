FROM openjdk:8-jdk-alpine
RUN addgroup -S alfredo && adduser -S alfredo -G alfredo
USER alfredo:alfredo
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]