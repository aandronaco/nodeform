package com.alfredo.nodeForm.persistence.dbto;

import com.alfredo.nodeForm.model.Node;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "node" )
@Data
@Accessors(chain = true)
@DynamicUpdate
public class NodeDBTO {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="parent_id")
    private NodeDBTO parent;

    @OneToMany( mappedBy = "parent", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true )
    private List<NodeDBTO> children = new ArrayList<>();
}
