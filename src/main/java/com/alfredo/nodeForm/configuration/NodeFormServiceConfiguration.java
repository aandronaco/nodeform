package com.alfredo.nodeForm.configuration;

import com.alfredo.nodeForm.controller.NodeFormController;
import com.alfredo.nodeForm.converter.NodeConverter;
import com.alfredo.nodeForm.persistence.repository.NodeRepository;
import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.NodeFormValidationService;
import com.alfredo.nodeForm.service.impl.NodeFormServiceImpl;
import com.alfredo.nodeForm.service.impl.NodeFormValidationServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(
        NodeFormPersistenceConfiguration.class
)
public class NodeFormServiceConfiguration {


    @Bean
    NodeFormService nodeFormService(
            NodeRepository nodeRepository,
            NodeConverter nodeConverter,
            NodeFormValidationService nodeFormValidationService
    ){
        return new NodeFormServiceImpl(nodeRepository, nodeConverter, nodeFormValidationService);
    }

    @Bean
    NodeConverter nodeConverter(){
        return new NodeConverter();
    }

    @Bean
    NodeFormValidationService nodeFormValidationService(NodeRepository nodeRepository){
        return new NodeFormValidationServiceImpl(nodeRepository);
    }


}
