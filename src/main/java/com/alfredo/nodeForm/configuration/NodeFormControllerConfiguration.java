package com.alfredo.nodeForm.configuration;

import com.alfredo.nodeForm.controller.NodeFormController;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        NodeFormController.class,
        NodeFormServiceConfiguration.class
})
public class NodeFormControllerConfiguration {



}
