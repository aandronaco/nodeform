package com.alfredo.nodeForm.configuration;

import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.impl.NodeFormServiceImpl;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.alfredo.nodeForm.persistence.repository")
@EntityScan("com.alfredo.nodeForm.persistence.dbto")
public class NodeFormPersistenceConfiguration {



}
