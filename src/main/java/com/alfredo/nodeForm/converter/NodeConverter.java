package com.alfredo.nodeForm.converter;

import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;

import java.util.ArrayList;
import java.util.List;

public class NodeConverter {

    public NodeDBTO toNodeDBTO(Node node, NodeDBTO parentNode) {
        NodeDBTO nodeDBTO = new NodeDBTO()
            .setName(node.getName())
            .setDescription(node.getDescription());
        if (parentNode!=null){
            nodeDBTO.setParent(parentNode);
        }
        return nodeDBTO;
    }

    public NodeDBTO mergeNodetoNodeDBTO(Node node, NodeDBTO oldNode, NodeDBTO parentNode) {
        return  oldNode.setParent(parentNode)
                        .setName(node.getName())
                        .setDescription(node.getDescription());

    }

    public Node toNode(NodeDBTO nodeDBTO, boolean loadChildren) {
        if (nodeDBTO==null) return null;
        Node node = new Node()
                .setId(nodeDBTO.getId())
                .setName(nodeDBTO.getName())
                .setDescription(nodeDBTO.getDescription())
                .setParent_id(nodeDBTO.getParent() != null ? nodeDBTO.getParent().getId() : null);
        if(loadChildren) {
            node.setNodes(toNodes(nodeDBTO.getChildren()));
        }
        return node;
    }

    private List<Node> toNodes(List<NodeDBTO> children) {
        if(children==null){
            return null;
        }

        List<Node> nodes = new ArrayList<>();
        for (NodeDBTO nodeDBTO : children){
            nodes.add(toNode(nodeDBTO, true));
        }
        return nodes;
    }
}
