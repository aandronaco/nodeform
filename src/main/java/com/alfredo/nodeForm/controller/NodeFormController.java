package com.alfredo.nodeForm.controller;


import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 *
 * @author alfredo
 */
@RestController
@RequestMapping( "/api" )
@Data
@Accessors(chain = true)
@Slf4j
public class NodeFormController {

    public static Logger logger = LoggerFactory.getLogger( NodeFormController.class );

    @Autowired
    NodeFormService nodeFormService;


    @RequestMapping( path = "/hello", method = GET )
    public String hello() {
        return "hello world";
    }

    @ApiOperation(value = "Create new node")
    @RequestMapping( path = "/node",
            consumes = MediaType.ALL_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = POST )
    public @ResponseBody Integer createNode(@RequestBody Node node) {
        log.info("request is: [{}]", node);
        try {
            return nodeFormService.createNode(node);
        } catch (NodeFormValidationException e) {
            log.error("Unable to create node: " +  e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Edit node")
    @RequestMapping( path = "/node",
            consumes = MediaType.ALL_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = PUT )
    public void editNode(@RequestBody Node node) {
        try {
            nodeFormService.editNode(node);
        } catch (NodeFormValidationException e) {
            log.error("Unable to edit node: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Retrieve single node without tree")
    @RequestMapping( path = "/node/{id}",
            consumes = MediaType.ALL_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = GET )
    public @ResponseBody  Node  editNode(@PathVariable(name = "id", required = true) Integer idNode) {
        try {
            return nodeFormService.retrieveNode(idNode);
        } catch (NodeFormValidationException e) {
            log.error("Unable to edit node: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Retrieve node tree")
    @RequestMapping( path = "/nodeTree/{id}",
            consumes = MediaType.ALL_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = GET )
    public @ResponseBody  Node getNodeTree(@PathVariable(name = "id", required = true) Integer idNode) {
        try {
            return nodeFormService.retrieveNodeTree(idNode);
        } catch (NodeFormValidationException e) {
            log.error("Unable to retrieve tree node: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
