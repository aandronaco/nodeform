package com.alfredo.nodeForm.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class Node {

   Integer id;
   String name;
   String description;
   Integer parent_id;
   List<Node> nodes;
}

