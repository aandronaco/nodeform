package com.alfredo.nodeForm.service;

import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;

public interface NodeFormValidationService {

    void validateCreateNode(Node node) throws NodeFormValidationException;

    void validateEditNodeId(Node node) throws NodeFormValidationException;

    void validateEditNode(Node node, NodeDBTO oldNode)  throws NodeFormValidationException;

    void validateRetrieve(Integer idNode) throws NodeFormValidationException;

    void validateRecursionProblem(Integer idNode, Integer idNodeParent) throws NodeFormValidationException;


}
