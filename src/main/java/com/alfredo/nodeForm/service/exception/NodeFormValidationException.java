package com.alfredo.nodeForm.service.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
public class NodeFormValidationException extends  Exception {


    public NodeFormValidationException(String message ) {
        super( message );
    }

    public NodeFormValidationException(String message, Exception e ) {
        super( message , e);
    }
}
