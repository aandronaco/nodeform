package com.alfredo.nodeForm.service;

import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;

public interface NodeFormService {

    Integer createNode(Node node) throws NodeFormValidationException;

    void editNode(Node node) throws NodeFormValidationException;

    Node retrieveNodeTree(Integer idNode) throws NodeFormValidationException;

    Node retrieveNode(Integer idNode) throws NodeFormValidationException;

}
