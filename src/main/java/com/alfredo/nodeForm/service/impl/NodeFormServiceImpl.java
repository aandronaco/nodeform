package com.alfredo.nodeForm.service.impl;

import com.alfredo.nodeForm.converter.NodeConverter;
import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;
import com.alfredo.nodeForm.persistence.repository.NodeRepository;
import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.NodeFormValidationService;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@Slf4j
@Transactional(propagation = Propagation.REQUIRED)
public class NodeFormServiceImpl implements NodeFormService {

    private NodeRepository nodeRepository;
    private NodeConverter nodeConverter;
    private NodeFormValidationService nodeFormValidationService;


    @Override
    public Integer createNode(Node node) throws NodeFormValidationException {
        log.info("create node: [{}]", node);
        nodeFormValidationService.validateCreateNode(node);
        NodeDBTO parentNode = null;
        if (node.getParent_id() != null)
            parentNode = nodeRepository.findById(node.getParent_id()).orElseThrow(() -> new NodeFormValidationException(String.format("Parent_id [%s] not found", node.getParent_id())));
        NodeDBTO savedNode = nodeRepository.save(nodeConverter.toNodeDBTO(node, parentNode));
        log.info("created node with id: [{}]", savedNode.getId() );
        return savedNode.getId();


    }

    @Override
    public void editNode(Node node) throws NodeFormValidationException {
        log.info("edit node: [{}]", node);

        nodeFormValidationService.validateEditNodeId(node);
        if(node.getParent_id()!=null) {
            nodeFormValidationService.validateRecursionProblem(node.getId(), node.getParent_id());
        }
        NodeDBTO oldNode = nodeRepository.findById(node.getId()).orElseThrow(() -> new NodeFormValidationException(String.format("Node with id [%s] not found", node.getId())));
        nodeFormValidationService.validateEditNode(node, oldNode);

        NodeDBTO parentNode = null;
        if (node.getParent_id() != null)
            parentNode = nodeRepository.findById(node.getParent_id()).orElseThrow(() -> new NodeFormValidationException(String.format("Parent_id [%s] not found", node.getParent_id())));
        NodeDBTO nodeToSave = nodeConverter.mergeNodetoNodeDBTO(node, oldNode, parentNode);
        log.info("edited node with id: [{}]", nodeToSave.getId() );

    }



    @Override
    public Node retrieveNodeTree(Integer idNode) throws NodeFormValidationException {
        log.info("start retrieve node tree with id: [{}]", idNode);
        nodeFormValidationService.validateRetrieve(idNode);
        NodeDBTO nodeDBTO = nodeRepository.findById(idNode).orElseThrow(() -> new NodeFormValidationException(String.format("Node with id [%s] not found", idNode)));
        return nodeConverter.toNode(nodeDBTO, true);
    }

    @Override
    public Node retrieveNode(Integer idNode) throws NodeFormValidationException {
        nodeFormValidationService.validateRetrieve(idNode);
        NodeDBTO nodeDBTO = nodeRepository.findById(idNode).orElseThrow(() -> new NodeFormValidationException(String.format("Node with id [%s] not found", idNode)));
        return nodeConverter.toNode(nodeDBTO, false);
    }
}
