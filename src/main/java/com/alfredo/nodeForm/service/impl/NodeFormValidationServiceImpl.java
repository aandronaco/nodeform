package com.alfredo.nodeForm.service.impl;

import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;
import com.alfredo.nodeForm.persistence.repository.NodeRepository;
import com.alfredo.nodeForm.service.NodeFormValidationService;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.List;

@AllArgsConstructor
public class NodeFormValidationServiceImpl implements NodeFormValidationService {


    private NodeRepository nodeRepository;


    @Override
    public void validateCreateNode(Node node) throws NodeFormValidationException {

        commonValidation(node);

        if (node.getId()!=null){
            throw new NodeFormValidationException("You are in creation. You can't fill id.");
        }

        if(!StringUtils.hasText(node.getName())){
            throw new NodeFormValidationException("Field name is mandatory");
        }

        if(node.getName().length()>50){
            throw new NodeFormValidationException("Field name cannot be more than 50 characters");
        }

    }


    @Override
    public void validateEditNodeId(Node node) throws NodeFormValidationException {

        if(node == null){
            throw new NodeFormValidationException("Node cannot be null");
        }

        if(node.getId()==null){
            throw new NodeFormValidationException("Field id cannot be null");
        }

        if(node.getParent_id()!=null && node.getParent_id().equals(node.getId())){
            throw new NodeFormValidationException("Field parent id cannot be the same as the node id");
        }

    }

    @Override
    public void validateEditNode(Node node, NodeDBTO oldNode) throws NodeFormValidationException {

        commonValidation(node);

        if(!oldNode.getName().equals(node.getName())){
            throw new NodeFormValidationException("Field name cannot be changed");
        }

    }

    @Override
    public void validateRetrieve(Integer idNode) throws NodeFormValidationException {
        if(idNode==null){
            throw new NodeFormValidationException("Field id cannot be null");
        }
    }

    @Override
    public void validateRecursionProblem(Integer idNode, Integer idNodeParent) throws NodeFormValidationException {
        if(idNode!=null) {
            List<NodeDBTO> parentList = nodeRepository.findByParentId(idNode);
            for (NodeDBTO nodeDBTO : parentList) {
                if (nodeDBTO.getId().equals(idNodeParent)){
                    throw new NodeFormValidationException("Recursion Problem. You can't insert node with parent id: " + idNodeParent);
                }
                validateRecursionProblem(nodeDBTO.getId(), idNodeParent);
            }
        }
    }


    private void commonValidation(Node node) throws NodeFormValidationException {

        if (node == null){
            throw new NodeFormValidationException("Node cannot be null");
        }

        if((!StringUtils.hasText(node.getDescription()))){
            throw new NodeFormValidationException("Field description is mandatory");
        }

        if(node.getDescription().length()>250){
            throw new NodeFormValidationException("Field name cannot be more than 50 characters");
        }
    }
}
