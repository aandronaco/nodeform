package com.alfredo.nodeForm.boot;

import com.alfredo.nodeForm.configuration.NodeFormControllerConfiguration;
import com.alfredo.nodeForm.configuration.NodeFormPersistenceConfiguration;
import com.alfredo.nodeForm.configuration.NodeFormServiceConfiguration;
import com.alfredo.nodeForm.configuration.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
		NodeFormControllerConfiguration.class,
		SwaggerConfiguration.class
})
public class NodeFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(NodeFormApplication.class, args);
	}

}
