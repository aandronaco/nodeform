package com.alfredo.nodeForm.service.impl;

import com.alfredo.nodeForm.boot.NodeFormApplication;
import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;
import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.fail;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = NodeFormApplication.class)
@DirtiesContext
@PropertySource("application-test.properties")
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class NodeFormServiceImplTest {
    
    @Autowired
    NodeFormService nodeFormService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    
    @Test
    @DirtiesContext
    public void testCreateNode() throws Exception {
        Integer idNode = nodeFormService.createNode(new Node().setName("myNode").setDescription("myDescription"));
        assertThat(idNode, is(not(nullValue())));

        Map<String, Object> nodeDBTO = jdbcTemplate.queryForMap("select * from node where id = "+ idNode);
        assertThat(nodeDBTO.get("name"), is("myNode"));
        assertThat(nodeDBTO.get("description"), is("myDescription"));
    }

    @Test
    @DirtiesContext
    @Sql(scripts = {"file:src/test/resources/h2/editNode.sql"})
    public void testCreateChildNode()throws Exception{
        Integer idNode = nodeFormService.createNode(new Node().setName("myNode").setDescription("myDescription").setParent_id(6));
        assertThat(idNode, is(not(nullValue())));

        Map<String, Object> nodeDBTO = jdbcTemplate.queryForMap("select * from node where id = "+ idNode);
        assertThat(nodeDBTO.get("name"), is("myNode"));
        assertThat(nodeDBTO.get("description"), is("myDescription"));
        assertThat(nodeDBTO.get("parent_id"), is(6));
    }

    @Test
    @DirtiesContext
    @Sql(scripts = {"file:src/test/resources/h2/editNode.sql"})
    public void testEditNode() throws Exception{
        nodeFormService.editNode(new Node().setId(6).setName("nodeInDb").setDescription("editedDescription"));


        Map<String, Object> nodeDBTO = jdbcTemplate.queryForMap("select * from node where id = 6");
        assertThat(nodeDBTO.get("name"), is("nodeInDb"));
        assertThat(nodeDBTO.get("description"), is("editedDescription"));

    }

    @Test
    @DirtiesContext
    @Sql(scripts = {"file:src/test/resources/h2/nodeTree.sql"})
    public void testRetrieveNodeTree() throws Exception{
        Node node = nodeFormService.retrieveNodeTree(6);
        assertThat(node.getName(), is("nodeInDb"));
        assertThat(node.getDescription(), is("description"));
    }

    @Test
    @DirtiesContext
    @Sql(scripts = {"file:src/test/resources/h2/nodeTree.sql"})
    public void testRetrieveNodeTreeComplex() throws Exception{
        Node node = nodeFormService.retrieveNodeTree(10);
        assertThat(node.getName(), is("nodeInDb10"));
        assertThat(node.getParent_id(), is(nullValue()));
        assertThat(node.getNodes(), is(not(nullValue())));
        assertThat(node.getNodes().size(), is(2));
        assertThat(node.getNodes().get(0).getName(), is("nodeInDb11"));
        assertThat(node.getNodes().get(1).getName(), is("nodeInDb12"));
        assertThat(node.getNodes().get(0).getNodes().size(), is(1));
        assertThat(node.getNodes().get(0).getNodes().get(0).getName(), is("nodeInDb13"));
    }

    @Test
    @DirtiesContext
    @Sql(scripts = {"file:src/test/resources/h2/nodeTree.sql"})
    public void testValidateRecursionProblem() throws Exception{
        try {
            nodeFormService.editNode(new Node().setId(10).setName("nodeInDb10").setDescription("d1").setParent_id(13));
            fail();
        }catch (NodeFormValidationException e){
            // test OK
            assertThat(e.getMessage(), is("Recursion Problem. You can't insert node with parent id: 13"));
        }

        try{
            nodeFormService.editNode(new Node().setId(10).setName("nodeInDb10").setDescription("d1").setParent_id(12));
            fail();
        }catch (NodeFormValidationException e){
            // test OK
            assertThat(e.getMessage(), is("Recursion Problem. You can't insert node with parent id: 12"));
        }

        nodeFormService.editNode(new Node().setId(12).setName("nodeInDb12").setDescription("d1").setParent_id(6));

    }

}