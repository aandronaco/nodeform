
package com.alfredo.nodeForm.service.impl;

import com.alfredo.nodeForm.boot.NodeFormApplication;
import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.service.NodeFormService;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = NodeFormApplication.class)
@PropertySource("application-test.properties")
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class NodeFormServiceImplIntegrationTest {
    
    @Autowired
    NodeFormService nodeFormService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    
    @Test
    @DirtiesContext
    public void testWorkflows() throws Exception {
        Node node = new Node().setName("myNode").setDescription("myDescription");
        Integer idNode = nodeFormService.createNode(node);
        assertThat(idNode, is(not(nullValue())));

        Map<String, Object> nodeDBTO = jdbcTemplate.queryForMap("select * from node where id = "+ idNode);
        assertThat(nodeDBTO.get("name"), is("myNode"));
        assertThat(nodeDBTO.get("description"), is("myDescription"));

        nodeFormService.editNode(node.setId(idNode).setDescription("editedDescription"));

        nodeDBTO = jdbcTemplate.queryForMap("select * from node where id = "+ idNode);
        assertThat(nodeDBTO.get("description"), is("editedDescription"));

        Node child1 = new Node().setName("child1").setDescription("childDesc1").setParent_id(idNode);
        Integer idNodeChild1 = nodeFormService.createNode(child1);
        child1.setId(idNodeChild1);
        Node child2 = new Node().setName("child2").setDescription("childDesc2").setParent_id(idNode);
        Integer idNodeChild2 = nodeFormService.createNode(child2);
        child2.setId(idNodeChild2);


        nodeFormService.editNode(child2.setDescription("childDesc2Edited"));

        nodeFormService.editNode(child1.setParent_id(idNodeChild2));

        Node finalNode = nodeFormService.retrieveNodeTree(idNode);
        assertThat(finalNode.getName(), is("myNode"));
        assertThat(finalNode.getDescription(), is("editedDescription"));
        assertThat(finalNode.getNodes().size(), is(1));
        assertThat(finalNode.getNodes().get(0).getName(), is("child2"));
        assertThat(finalNode.getNodes().get(0).getDescription(), is("childDesc2Edited"));
        assertThat(finalNode.getNodes().get(0).getNodes().get(0).getName(), is("child1"));
    }


}