package com.alfredo.nodeForm.service.impl;

import com.alfredo.nodeForm.model.Node;
import com.alfredo.nodeForm.persistence.dbto.NodeDBTO;
import com.alfredo.nodeForm.service.exception.NodeFormValidationException;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class NodeFormValidationServiceImplTest {

    NodeFormValidationServiceImpl nodeFormValidationService = new NodeFormValidationServiceImpl(null);

    @Test
    public void testValidateCreateNode() throws Exception {
        assertThatCreateNodeValidation(null, "Node cannot be null");
        assertThatCreateNodeValidation(new Node(), "Field description is mandatory");
        assertThatCreateNodeValidation(new Node().setName("myNode"), "Field description is mandatory");
        assertThatCreateNodeValidation(new Node().setDescription("myDesc"), "Field name is mandatory");
        nodeFormValidationService.validateCreateNode(new Node().setName("myNode").setDescription("myDesc"));
    }

    private void assertThatCreateNodeValidation(Node node, String message) {
        try {
            nodeFormValidationService.validateCreateNode(node);
            fail("Error!! Validation not working");
        }catch (NodeFormValidationException e){
            assertThat(e.getMessage(), is(message));
        }
    }

    @Test
    public void testValidateEditNode() throws Exception {

        NodeDBTO oldNode = new NodeDBTO()
                .setId(1)
                .setName("nameInDb")
                .setDescription("descriptionInDb")
                .setParent(new NodeDBTO().setId(2))
                ;

        assertThatEditNodeValidation(null, oldNode, "Node cannot be null");
        assertThatEditNodeValidation(new Node(), oldNode,"Field description is mandatory");
        assertThatEditNodeValidation(new Node().setDescription("newDescription"), oldNode,"Field name cannot be changed");
        nodeFormValidationService.validateEditNode( new Node().setName("nameInDb").setDescription("newDescription"), oldNode);
    }

    private void assertThatEditNodeValidation(Node node, NodeDBTO oldNode,  String message) {
        try {

            nodeFormValidationService.validateEditNode(node, oldNode);
            fail("Error!! Validation not working");
        }catch (NodeFormValidationException e){
            assertThat(e.getMessage(), is(message));
        }
    }
}